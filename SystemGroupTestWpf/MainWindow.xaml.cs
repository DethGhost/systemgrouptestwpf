﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace SystemGroupTestWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const char WordsSeparator = ' ';

        private readonly string[] _charsToClear = new[]
        {
            "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "+", "=", "-", "/", ".", ",", "\\", "`", "'", "[",
            "]", "{", "}", "\"", ";", ":", "?", "|", "№", "\r", "\n", "\t", "<", ">"
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CountWords(object sender, RoutedEventArgs eventArguments)
        {
            string text = TextInputBox.Text;

            if (string.IsNullOrEmpty(text))
            {
                MessageBox.Show("You need to enter some text before try count words");

                return;
            }

            Dictionary<string, int> result = CountWordsInText(text);
            var info = new InfoWindow(result) {Owner = this};
            info.Show();
        }

        private Dictionary<string, int> CountWordsInText(string text)
        {
            foreach (string char_ in _charsToClear)
            {
                text = text.Replace(char_, " ");
            }

            IList<string> words = text.Split(WordsSeparator);
            var result = new Dictionary<string, int>();

            foreach (string word in words)
            {
                if (string.IsNullOrEmpty(word) || result.ContainsKey(word))
                {
                    continue;
                }

                int wordCountInText = words.Count(x => x == word);
                result.Add(word, wordCountInText);
            }

            return result.OrderByDescending(x => x.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }
}
