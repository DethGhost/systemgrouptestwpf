﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace SystemGroupTestWpf
{
    public partial class InfoWindow : Window
    {
        public ObservableCollection<KeyValuePair<string, int>> Data { get; }

        public InfoWindow(Dictionary<string, int> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            this.Data = new ObservableCollection<KeyValuePair<string, int>>(data);
            InitializeComponent();
        }


    }
}
